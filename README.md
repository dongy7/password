# RNN Password Vault Cracker

This project uses recurrent neural networks (RNNs) trained on decoy and real password data to distinguish decoy password vaults
from real vaults. For details, check out the project [paper](https://github.com/dongy7/password/blob/master/Cracking_Password_Vaults_RNN.pdf).
